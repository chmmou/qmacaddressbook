/*
 * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of QMacAddressBook.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QMACADDRESSBOOK_H
#define QMACADDRESSBOOK_H

#include "qmacaddressbook_global.h"

#include <QObject>
#include <QPixmap>

namespace qmacaddressbook {

  /**
   * @brief The QMacAddressBookDelegate class
   */
  class QMACADDRESSBOOK_EXPORT QMacAddressBookDelegate {

    public:
      /**
       * @brief personAdded
       * @param person
       */
      virtual void personAdded(const Person *person) = 0;

      /**
       * @brief personUpdated
       * @param person
       */
      virtual void personUpdated(const Person *person) = 0;

      /**
       * @brief personRemoved
       * @param person
       */
      virtual void personRemoved(const Person *person) = 0;

      /**
       * @brief groupAdded
       * @param group
       */
      virtual void groupAdded(const PersonGroup *group) = 0;

      /**
       * @brief groupUpdated
       * @param group
       */
      virtual void groupUpdated(const PersonGroup *group) = 0;

      /**
       * @brief groupRemoved
       * @param group
       */
      virtual void groupRemoved(const PersonGroup *group) = 0;

      /**
       * @brief addressBookReloaded
       */
      virtual void addressBookReloaded() = 0;
  };

  class QMacAddressBookPrivate;

  /**
   * @brief The QMacAddressBook class
   */
  class QMACADDRESSBOOK_EXPORT QMacAddressBook : public QObject {

    public:
      /**
       * @brief Default constructor for @class QMacAddressBook
       *
       * @param parent The parent @class QObject
       * @param delegate
       */
      QMacAddressBook(QObject *parent = 0, const QMacAddressBookDelegate *delegate = 0);

      /**
       * Default de-constructor
       */
      ~QMacAddressBook();

      /**
       * @brief setDelegate
       * @param delegate
       */
      void setAddressBookDelegate(const QMacAddressBookDelegate *delegate);

      /**
       * @brief Set it to true if you would to save automatically
       *
       * @param autosave True for automatically saving
       */
      void setAutoSaving(const bool autosave = true);

      /**
       * @brief Saves all the changes made since the last save.
       *
       * @return true if this save is successful or if there were no changes, false otherwise.
       */
      bool save();

      /**
       * @brief Get all persons from Mac OS X Address Book
       *
       * @return Returns a @class QList with @struct Person
       */
      const QList<Person *> persons() const;

      /**
       * @brief personsForGroup
       * @param group
       * @return
       */
      const QList<Person *> personsForGroup(const QString &name) const;

      /**
       * @brief personsForName
       * @param name
       * @return
       */
      const QList<Person *> personsForName(const QString &name,
                                           const SearchComparisonProperty comparisonProperty = FirstName,
                                           const SearchComparison comparison = ContainsCaseInsensitive) const;

      /**
       * @brief personProperties
       * @return
       */
      const QList<PersonProperty *> personProperties() const;

      /**
       * @brief addPerson
       * @param person
       */
      void addPerson(const Person *person);

      /**
       * @brief addPersonToGroup
       * @param person
       * @param group
       */
      void addPersonToGroup(const Person *person, const PersonGroup *group);

      /**
       * @brief updatePerson
       * @param person
       */
      void updatePerson(const Person *person);

      /**
       * @brief removePerson
       * @param person
       */
      void removePerson(const Person *person);

      /**
       * @brief removePersonFromGroup
       * @param person
       * @param group
       */
      void removePersonFromGroup(const Person *person, const PersonGroup *group);

      /**
       * @brief movePersonFromGroupToGroup
       * @param person
       * @param oldGroup
       * @param newGroup
       */
      void movePersonFromGroupToGroup(const Person *person, const PersonGroup *oldGroup, const PersonGroup *newGroup);

      /**
       * @brief personImage
       * @param person
       * @return
       */
      const QPixmap personImage(const Person *person, const QPixmap &defaultImage);

      /**
       * @brief addPersonImage
       * @param person
       * @param image
       */
      void addPersonImage(const Person *person, const QPixmap &pixmap);

      /**
       * @brief groups
       * @return
       */
      const QList<PersonGroup *> groups() const;

      /**
       * @brief groupProperties
       * @return
       */
      const QList<GroupProperty *> groupProperties() const;

      /**
       * @brief addGroup
       * @param group
       */
      void addGroup(const PersonGroup *group);

      /**
       * @brief updateGroup
       * @param group
       */
      void updateGroup(const PersonGroup *group);

      /**
       * @brief removeGroup
       * @param group
       * @param personIncluded
       */
      void removeGroup(const PersonGroup *group, const bool personIncluded = false);

      /**
       * @brief Provide to release the @struct Person
       *
       * @param person The @struct Person to release
       */
      void releasePerson(Person *person);

    private:
      QMacAddressBookPrivate *d;
      QMacAddressBookDelegate *abDelegate;
  };
}

#endif // QMACADDRESSBOOK_H
