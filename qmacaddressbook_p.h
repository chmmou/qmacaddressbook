/*
 * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of QMacAddressBook.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QMACADDRESSBOOK_PRIVATE_H
#define QMACADDRESSBOOK_PRIVATE_H

#include "qmacaddressbook_global.h"

#include <AppKit/AppKit.h>
#include <Foundation/Foundation.h>

// That is a liitle hack to provide support for Mac AB Properties like kABFirstNameProperty ...
#if defined(__OBJC__)
#undef __OBJC__
#endif

#include <AddressBook/ABTypedefs.h>
#include <AddressBook/ABGlobalsC.h>
#include <AddressBook/ABAddressBookC.h>

namespace qmacaddressbook {

  class QMacAddressBookPrivate {

    public:
      QMacAddressBookPrivate();
      ~QMacAddressBookPrivate();

      /**
       * @brief Get all person from address book
       *
       * @return Returns a arry with all person
       */
      const CFArrayRef persons() const;

      /**
       * @brief Get all groups from address book
       *
       * @return Returns a arry with all groups
       */
      const CFArrayRef groups() const;

      /**
       * @brief personForRecord
       * @param personRecord
       * @return
       */
      Person *personForRecord(ABRecordRef personRecord);

      /**
       * @brief groupForRecord
       * @param groupRecord
       * @return
       */
      PersonGroup *groupForRecord(const ABGroupRef groupRecord);

      /**
       * @brief personsForName
       * @param value
       * @param comparisonProperty
       * @param comparison
       * @return
       */
      const CFArrayRef personsForName(const CFStringRef value,
                                     const SearchComparisonProperty comparisonProperty = FirstName,
                                     const SearchComparison comparison = ContainsCaseInsensitive) const;

      /**
       * @brief personsForGroup
       * @param property
       * @param group
       * @param comparison
       * @return
       */
      const CFArrayRef personsForGroup(const CFStringRef property,
                                      const CFStringRef group,
                                      const SearchComparison comparison = ContainsCaseInsensitive) const;


      /**
       * @brief properties
       * @param recordType
       * @return
       */
      const CFArrayRef properties(const CFStringRef recordType = kABPersonRecordType) const;

      /**
       * @brief addRecord
       * @param personRecord
       * @return
       */
      bool addPersonRecord(const ABRecordRef personRecord) const;

      /**
       * @brief addGroupRecord
       * @param groupRecord
       * @return
       */
      bool addGroupRecord(const ABGroupRef groupRecord) const;

      /**
       * @brief updatePesonRecord
       * @param person
       * @return
       */
      Person *preparePersonRecord(const Person *person);

      /**
       * @brief prepareGroupRecord
       * @param group
       * @return
       */
      PersonGroup *prepareGroupRecord(const PersonGroup *group);

      /**
       * @brief setPersonToRecord
       * @param person
       */
      void setPersonToRecord(const Person *person);

      /**
       * @brief setGroupToRecord
       * @param group
       */
      void setGroupToRecord(const PersonGroup *group);

      /**
       * @brief hasUnsavedChanges
       * @return
       */
      bool hasUnsavedChanges();

      /**
       * @brief save
       * @return
       */
      bool save();

      /**
       * @brief autoSaving
       */
      bool autoSaving;

    public:
      /**
       * @brief qstringFromCFStringRef
       * @param string
       * @return
       */
      QString qstringFromCFStringRef(CFStringRef string);

      /**
       * @brief qstringToCFStringRef
       * @param string
       * @return
       */
      CFStringRef qstringToCFStringRef(const QString &string);

      /**
       * @brief releaseCFObjects
       * @param object
       */
      void releaseCFObjects(CFTypeRef object);

    private:
      ABAddressBookRef addressBook;
      NSAutoreleasePool *releasePool;

      const QDate qdateFromCFDateRef(CFDateRef date) const;
      const CFStringRef searchComparisonProperty(const SearchComparisonProperty comparisonProperty) const;

      void personNames(Person *person, const ABRecordRef personRecord);
      void personAddress(Person *person, const ABRecordRef personRecord);
      void personPhones(Person *person, const ABRecordRef personRecord);
      void personUrls(Person *person, const ABRecordRef personRecord);
      void personService(Person *person, const ABRecordRef personRecord);
      void personInformation(Person *person, const ABRecordRef personRecord);
      void personDates(Person *person, const ABRecordRef personRecord);
      void personGroup(Person *person);

      void setPersonNames(Person *person, const ABRecordRef record);
      void setPersonAddress(Person *person, const ABRecordRef record);
      void setPersonPhones(Person *person, const ABRecordRef record);
      void setPersonService(Person *person, const ABRecordRef record);
      void setPersonDates(Person *person, const ABRecordRef record);
      void setPersonInformation(Person *person, const ABRecordRef record);
      void setPersonGroup(Person *person, const ABRecordRef record);

#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_7
    private:
      void personServiceInternal(Person *person, const ABRecordRef personRecord, const CFStringRef property);
#endif
  };
}

#endif // QMACADDRESSBOOK_PRIVATE_H
