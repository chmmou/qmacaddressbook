/*
 * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of QMacAddressBook.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "qmacaddressbook.h"
#include "qmacaddressbook_p.h"

namespace qmacaddressbook {

  QMacAddressBook::QMacAddressBook(QObject *parent, const QMacAddressBookDelegate *delegate)
    : QObject(parent), d(new QMacAddressBookPrivate), abDelegate(const_cast<QMacAddressBookDelegate *>(delegate)) {
  }

  QMacAddressBook::~QMacAddressBook() {

    abDelegate = 0;
    delete d;
  }

  void QMacAddressBook::setAddressBookDelegate(const QMacAddressBookDelegate *delegate) {

    if (!abDelegate) {
      abDelegate = const_cast<QMacAddressBookDelegate *>(delegate);
    }
    else {
      abDelegate = 0;
      abDelegate = const_cast<QMacAddressBookDelegate *>(delegate);
    }
  }

  void QMacAddressBook::setAutoSaving(const bool autosave) {
    d->autoSaving = autosave;
  }

  bool QMacAddressBook::save() {

    if (d->hasUnsavedChanges()) {
      return d->save();
    }

    return d->hasUnsavedChanges();
  }

  const QList<Person *> QMacAddressBook::persons() const {

    QList<Person *> mPersons = QList<Person *>();
    CFArrayRef personsRef = d->persons();
    const CFIndex personIndex = CFArrayGetCount(personsRef);
    for (CFIndex a = 0; a < personIndex; ++a) {
      mPersons.append(d->personForRecord((ABRecordRef)CFArrayGetValueAtIndex(personsRef, a)));
    }
    personsRef = 0x0;

    return mPersons;
  }

  const QList<Person *> QMacAddressBook::personsForGroup(const QString &name) const {

    QList<Person *> mGroupPersons = QList<Person *>();

    CFArrayRef groupsRef = d->groups();
    const CFIndex groupCount = CFArrayGetCount(groupsRef);
    for (CFIndex a = 0; a < groupCount; ++a) {

      ABGroupRef groupRecordRef = (ABGroupRef)CFArrayGetValueAtIndex(groupsRef, a);
      if (groupRecordRef) {

        QString groupName = d->qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(groupRecordRef, kABGroupNameProperty));
        if (name == groupName) {

          CFArrayRef groupMembersRef = ABGroupCopyArrayOfAllMembers(groupRecordRef);
          if (groupMembersRef) {

            const CFIndex groupMemberIndex = CFArrayGetCount(groupMembersRef);
            for (CFIndex b = 0; b < groupMemberIndex; ++b) {

              ABRecordRef personRecord = (ABRecordRef)CFArrayGetValueAtIndex(groupMembersRef, a);
              if (personRecord) {
                mGroupPersons.append(d->personForRecord(personRecord));
              }
            }
            d->releaseCFObjects(groupMembersRef);
          }
        }
        groupName.clear();
        groupName = QString::null;

        d->releaseCFObjects(groupRecordRef);
      }
    }
    d->releaseCFObjects(groupsRef);

    return mGroupPersons;
  }

  const QList<Person *> QMacAddressBook::personsForName(const QString &name, const SearchComparisonProperty comparisonProperty, const SearchComparison comparison) const {

    QList<Person *> searchResult = QList<Person *>();

    CFStringRef value = d->qstringToCFStringRef(name);
    CFArrayRef searchResultRef = d->personsForName(value, comparisonProperty, comparison);
    const CFIndex personIndex = CFArrayGetCount(searchResultRef);
    for (CFIndex a = 0; a < personIndex; ++a) {
      searchResult.append(d->personForRecord((ABRecordRef)CFArrayGetValueAtIndex(searchResultRef, a)));
    }
    d->releaseCFObjects(value);
    d->releaseCFObjects(searchResultRef);

    return searchResult;
  }

  const QList<PersonProperty *> QMacAddressBook::personProperties() const {

    QList<PersonProperty *> mPersonProperties = QList<PersonProperty *>();

    CFArrayRef propertyRef = d->properties(kABPersonRecordType);
    const CFIndex propertyCount = CFArrayGetCount(propertyRef);
    for (CFIndex a = 0; a < propertyCount; ++a) {

      CFStringRef property = (CFStringRef)CFArrayGetValueAtIndex(propertyRef, a);
      CFStringRef propertyLabel = ABCopyLocalizedPropertyOrLabel(property);

      PersonProperty *personProperty = new PersonProperty;
      personProperty->property = d->qstringFromCFStringRef(property);
      personProperty->lable = d->qstringFromCFStringRef(propertyLabel);

      mPersonProperties.append(personProperty);
    }

    d->releaseCFObjects(propertyRef);

    return mPersonProperties;
  }

  void QMacAddressBook::addPerson(const Person *person) {

    if (!person) {
      return;
    }

    Person *_person = d->preparePersonRecord(person);
    if (_person) {

      d->setPersonToRecord(_person);
      const bool added = d->addPersonRecord((ABRecordRef)_person->personRecord);
      if (added) {
        if (d->autoSaving) {
          save();
        }

        if (abDelegate) {
          abDelegate->personAdded(_person);
        }
      }

      d->releaseCFObjects(_person->personRecord);
      d->releaseCFObjects(_person->personRecordId);
      d->releaseCFObjects(_person->personPropertyId);

      releasePerson(_person);
    }
  }

  void QMacAddressBook::addPersonToGroup(const Person *person, const PersonGroup *group) {

    if (!person) {
      return;
    }

    if (person->personRecord) {

      if (group && !group->groupRecord) {
        PersonGroup *_group = d->prepareGroupRecord(group);

        CFStringRef value = d->qstringToCFStringRef(group->name);
        bool created = ABRecordSetValue((ABGroupRef)_group->groupRecord, kABGroupNameProperty, value);
        bool added = d->addGroupRecord((ABGroupRef)_group->groupRecord);

        d->releaseCFObjects(value);
      }

      if (d->autoSaving) {
        save();
      }
    }
  }

  void QMacAddressBook::updatePerson(const Person *person) {

    if (d->autoSaving) {
      save();
    }
  }

  void QMacAddressBook::removePerson(const Person *person) {

    if (d->autoSaving) {
      save();
    }
  }

  void QMacAddressBook::removePersonFromGroup(const Person *person, const PersonGroup *group) {

    if (d->autoSaving) {
      save();
    }
  }

  void QMacAddressBook::movePersonFromGroupToGroup(const Person *person, const PersonGroup *oldGroup, const PersonGroup *newGroup) {

    if (d->autoSaving) {
      save();
    }
  }

  const QPixmap QMacAddressBook::personImage(const Person *person, const QPixmap &defaultImage) {

    if (person->personRecord) {

      CFDataRef imageDataRef = ABPersonCopyImageData((ABPersonRef)person->personRecord);
      if (imageDataRef) {

        NSImage *nsImage = [[NSImage alloc] initWithData:(NSData*)imageDataRef];
        QPixmap pixmap = QPixmap::fromMacCGImageRef([nsImage CGImageForProposedRect:NULL context:nil hints:nil]);
        [nsImage release];

        d->releaseCFObjects(imageDataRef);

        if (pixmap.isNull()) {
          return defaultImage;
        }

        return pixmap;
      }
    }

    return defaultImage;
  }

  void QMacAddressBook::addPersonImage(const Person *person, const QPixmap &pixmap) {

    if (!pixmap.isNull()) {

      if (person->personRecord) {

        CGDataProviderRef cgDataProviderRef = CGImageGetDataProvider(pixmap.toMacCGImageRef());
        if (cgDataProviderRef) {

          CFDataRef imageDataRef = CGDataProviderCopyData(cgDataProviderRef);
          if (imageDataRef) {

            bool imageDataSet = ABPersonSetImageData((ABPersonRef)person->personRecord, imageDataRef);
            if (imageDataSet) {
              if (d->hasUnsavedChanges()) {
                d->save();
              }
            }

            d->releaseCFObjects(imageDataRef);
          }

          d->releaseCFObjects(cgDataProviderRef);
        }
      }
    }
  }

  const QList<PersonGroup *> QMacAddressBook::groups() const {

    QList<PersonGroup *> mGroups = QList<PersonGroup *>();

    CFArrayRef groupsRef = d->groups();
    const CFIndex groupIndex = CFArrayGetCount(groupsRef);
    for (CFIndex a = 0; a < groupIndex; ++a) {
      mGroups.append(d->groupForRecord((ABGroupRef)CFArrayGetValueAtIndex(groupsRef, a)));
    }

    d->releaseCFObjects(groupsRef);

    return mGroups;
  }

  const QList<GroupProperty *> QMacAddressBook::groupProperties() const {

    QList<GroupProperty *> mPersonGroupProperties = QList<GroupProperty *>();

    CFArrayRef propertyRef = d->properties(kABGroupRecordType);
    const CFIndex propertyCount = CFArrayGetCount(propertyRef);
    for (CFIndex a = 0; a < propertyCount; ++a) {

      CFStringRef property = (CFStringRef)CFArrayGetValueAtIndex(propertyRef, a);
      CFStringRef propertyLabel = ABCopyLocalizedPropertyOrLabel(property);

      GroupProperty *groupProperty = new GroupProperty;
      groupProperty->property = d->qstringFromCFStringRef(property);
      groupProperty->lable = d->qstringFromCFStringRef(propertyLabel);

      mPersonGroupProperties.append(groupProperty);
    }

    d->releaseCFObjects(propertyRef);

    return mPersonGroupProperties;
  }

  void QMacAddressBook::addGroup(const PersonGroup *group) {

    if (d->autoSaving) {
      save();
    }
  }

  void QMacAddressBook::updateGroup(const PersonGroup *group) {

    if (d->autoSaving) {
      save();
    }
  }

  void QMacAddressBook::removeGroup(const PersonGroup *group, const bool personIncluded) {

    if (d->autoSaving) {
      save();
    }
  }

  void QMacAddressBook::releasePerson(Person *person) {

    if (person) {

      if (person->names) {
        delete person->names;
        person->names = 0;
      }

      if (person->groups.count() > 0) {
        qDeleteAll(person->groups);
        person->groups.clear();
      }

      if (person->addresses.count() > 0) {
        qDeleteAll(person->addresses);
        person->addresses.clear();
      }

      if (person->services.count() > 0) {
        qDeleteAll(person->services);
        person->services.clear();
      }

      if (person->phones.count() > 0) {
        qDeleteAll(person->phones);
        person->phones.clear();
      }

      if (person->urls.count() > 0) {
        qDeleteAll(person->urls);
        person->urls.clear();
      }

      delete person;
      person = 0;
    }
  }

}
