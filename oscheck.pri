#/*
# * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * This file is part of QMacAddressBook.
# *
# * This library is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published
# * by the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this library.  If not, see <http://www.gnu.org/licenses/>.
# *
# */

# This functionality comes from Qt Creator
defineTest(minQtVersion) {

  maj = $$1
  min = $$2
  patch = $$3

  isEqual(QT_MAJOR_VERSION, $$maj) {
    isEqual(QT_MINOR_VERSION, $$min) {
      isEqual(QT_PATCH_VERSION, $$patch) {
        return(true)
      }
      greaterThan(QT_PATCH_VERSION, $$patch) {
        return(true)
      }
    }
    greaterThan(QT_MINOR_VERSION, $$min) {
      return(true)
    }
  }
  return(false)
}

# function to check Mac OS X Version
macx {

  defineTest(minMacVersion) {

    MAC_VERSION = $$system(sw_vers -productVersion)

    MAC_MAJOR_VERSION = $$section(MAC_VERSION, ".", 0, 0)
    MAC_MINOR_VERSION = $$section(MAC_VERSION, ".", 1, 1)
    MAC_PATCH_VERSION = $$section(MAC_VERSION, ".", 2, 2)

    isEmpty(MAC_PATCH_VERSION) {
      MAC_PATCH_VERSION = 0
    }

    maj = $$1
    min = $$2
    patch = $$3
    
    isEqual(MAC_MAJOR_VERSION, $$maj) {
      isEqual(MAC_MINOR_VERSION, $$min) {
        isEqual(MAC_PATCH_VERSION, $$patch) {
          return(true)
        }
        greaterThan(MAC_PATCH_VERSION, $$patch) {
          return(true)
        }
      }
      greaterThan(MAC_MINOR_VERSION, $$min) {
        return(true)
      }
    }
    return(false)
  }

  minMacVersion(10, 8, 0) {
    # Qt can build under OS X Mountain Lion but we use the old one OS X SDK
    QMAKE_MAC_SDK = /Developer/SDKs/MacOSX10.7.sdk
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
  }
  else {
    minMacVersion(10, 7, 0) {
      # Qt must build with Mac OS X Lion
      QMAKE_MAC_SDK = /Developer/SDKs/MacOSX10.7.sdk
      QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7
    }
    else {
      minMacVersion(10, 6, 8) {
        # Qt must build with Mac OS X Snow Leopard
        QMAKE_MAC_SDK = /Developer/SDKs/MacOSX10.6.sdk
        QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.6
      }
      else {
        message("Can't build with Mac OS X $${MAC_VERSION}.")
        error("Use at least Mac OS X 10.6.8.")
      }
    }
  }
}

CONFIG(x86_64) {
  CONFIG *= x86_64
} else {
  CONFIG *= x86
}
