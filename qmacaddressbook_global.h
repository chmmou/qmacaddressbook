/*
 * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of QMacAddressBook.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QMACADDRESSBOOK_GLOBAL_H
#define QMACADDRESSBOOK_GLOBAL_H

#include <CoreFoundation/CoreFoundation.h>

typedef const void *RecordRef;
typedef CFStringRef RecordID;
typedef CFStringRef PropertyID;

#include <QDate>
#include <QList>
#include <QString>

#if defined(QMACADDRESSBOOK_LIB)
#  if defined(MAKE_QMACADDRESSBOOK_LIB)
#    define QMACADDRESSBOOK_EXPORT Q_DECL_EXPORT
#  endif
#else
#  define QMACADDRESSBOOK_EXPORT Q_DECL_IMPORT
#endif

namespace qmacaddressbook {

  /**
   * @brief SearchComparison
   */
  enum QMACADDRESSBOOK_EXPORT SearchComparison {
    Equal,
    EqualCaseInsensitive,
    Contains,
    ContainsCaseInsensitive
  };

  /**
   * @brief SearchComparisonProperty
   */
  enum QMACADDRESSBOOK_EXPORT SearchComparisonProperty {
    FirstName,
    LastName,
    FirstNamePhonetic,
    LastNamePhonetic,
    Nickname,
    MaidenName,
    Organization,
    JobTitle,
    HomePage,
    Department,
    MiddleName,
    MiddleNamePhonetic,
    Title,
    Suffix
  };

  /**
   * @brief The PersonGroup struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonGroup {

    QString name;

    /** Internal usage */
    RecordRef groupRecord;
    RecordID groupRecordId;
    RecordID groupPropertyId;
  };

  /**
   * @brief The PersonGroupProperty struct
   */
  struct QMACADDRESSBOOK_EXPORT GroupProperty {

    QString property;
    QString lable;
  };

  /**
   * @brief The PersonProperty struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonProperty {

    QString property;
    QString lable;
  };

  /**
   * @brief The PersonAddress struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonAddress {

    QString completeAddress;
    QString street;
    QString city;
    QString state;
    QString zipCode;
    QString country;
    QString countryCode;
    QString label;
  };

  /**
   * @brief The PersonDate struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonDate {

    QDate date;
    QString label;
  };

  /**
   * @brief The PersonService struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonService {

#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_7
    QString aim;
    QString jabber;
    QString msn;
    QString yahoo;
    QString icq;
#else
    QString userName;
    QString service;
    QString aim;
    QString facebook;
    QString gadugadu;
    QString googleTalk;
    QString icq;
    QString jabber;
    QString msn;
    QString qq;
    QString skype;
    QString yahoo;
#endif

    QString label;
  };

  /**
   * @brief The PersonNames struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonNames {

    QString firstName;
    QString lastName;
    QString middleName;
    QString prefix;
    QString suffix;
    QString nickName;
    QString firstNamePhonetic;
    QString lastNamePhonetic;
    QString middleNamePhonetic;
  };

  /**
   * @brief The PersonInformation struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonInformation {

    QString organization;
    QString jobTitle;
    QString department;
    QString note;
    QDate date;
    QDate birthday;

    /** It is automatically set */
    QDate creationDate;

    /** It is automatically set */
    QDate modificationDate;

    /** Set it to true if person / people a company */
    bool isCompany;
  };

  /**
   * @brief The PersonPhone struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonPhone {

    QString number;
    QString label;
  };

  /**
   * @brief The PersonUrl struct
   */
  struct QMACADDRESSBOOK_EXPORT PersonUrl {

    QString hompage;
    QString email;
    QString label;
  };

  /**
   * @brief The Person struct
   */
  struct QMACADDRESSBOOK_EXPORT Person {

    PersonNames *names;
    PersonInformation *information;

    QList<PersonGroup *> groups;
    QList<PersonAddress *> addresses;
    QList<PersonService *> services;
    QList<PersonPhone *> phones;
    QList<PersonUrl *> urls;
    QList<PersonDate *> dates;

    /** Internal usage */
    RecordRef personRecord;
    RecordID personRecordId;
    RecordID personPropertyId;
  };
}

#endif // QMACADDRESSBOOK_GLOBAL_H
