#/*
# * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * This file is part of QMacAddressBook.
# *
# * This library is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published
# * by the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this library.  If not, see <http://www.gnu.org/licenses/>.
# *
# */

include(QMacAddressBook.pri)
include(oscheck.pri)

# Qt 4 Settings
TEMPLATE = lib
DESTDIR	= build/bin
TARGET = QMacAddressBook
CONFIG += qt thread warn_on lib_bundle silent

LIBS += -framework AddressBook
LIBS += -framework AppKit
LIBS += -framework CoreFoundation
LIBS += -framework Foundation

VERSIONS = 1
DEFINE += QMACADDRESSBOOK_LIB
DEFINE += MAKE_QMACADDRESSBOOK_LIB

QMAKE_LFLAGS_SONAME  = -Wl,-install_name,@executable_path/../Frameworks/
QMAKE_INFO_PLIST = Info.plist

FRAMEWORK_HEADERS.version = Versions
FRAMEWORK_HEADERS.files = qmacaddressbook_global.h qmacaddressbook.h

FRAMEWORK_HEADERS.path = Headers
QMAKE_BUNDLE_DATA += FRAMEWORK_HEADERS

# Config settings
CONFIG(debug, debug|release) {

  RCC_DIR = build/$${TARGET}/debug/rcc
  MOC_DIR += build/$${TARGET}/debug/moc
  OBJECTS_DIR += build/$${TARGET}/debug/obj

  DEFINES += QMACADDRESSBOOK_DEBUG

} else {

  RCC_DIR = build/$${TARGET}/release/rcc
  MOC_DIR += build/$${TARGET}/release/moc
  OBJECTS_DIR += build/$${TARGET}/release/obj

  DEFINES -= QMACADDRESSBOOK_DEBUG
}

# Qt check and we use the newest one
!minQtVersion(4, 8, 1) {

  message("Can't build $${TARGET} with Qt Version $${QT_VERSION}.")
  error("Use at least Qt 4.8.1.")
}
