#/*
# * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * This file is part of QMacAddressBook.
# *
# * This library is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published
# * by the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this library.  If not, see <http://www.gnu.org/licenses/>.
# */

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += qmacaddressbook_global.h
HEADERS += qmacaddressbook.h

# Mac OS X - Objective C++ source files
OBJECTIVE_SOURCES += qmacaddressbook.mm
OBJECTIVE_SOURCES += qmacaddressbook_p.mm
OBJECTIVE_HEADERS += qmacaddressbook_p.h
