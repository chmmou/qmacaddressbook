/*
 * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of QMacAddressBook.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "examplewidget.h"
#include "ui_examplewidget.h"

#include <qmacaddressbook.h>

#if defined(EXAMPLE_DEBUG)
#include <QDebug>
#endif

using namespace qmacaddressbook;

static QMacAddressBook *adb = 0;

ExampleWidget::ExampleWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::ExampleWidget) {

  ui->setupUi(this);

  adb = new QMacAddressBook(this);
  adb->setAutoSaving();

  QList<Person *> persons = adb->persons();
  Q_FOREACH (Person *person, persons) {

    QList<PersonAddress *> addresses = person->addresses;
    Q_FOREACH (const PersonAddress *address, addresses) {
#if defined(EXAMPLE_DEBUG)
      qDebug() << address->label;
#endif
    }

    adb->releasePerson(person);
  }
  persons.clear();

  QList<PersonGroup *> groups = adb->groups();
  Q_FOREACH (PersonGroup *group, groups) {

#if defined(EXAMPLE_DEBUG)
    qDebug() << "Group: " << group->name;
#endif

  }
  qDeleteAll(groups);
  groups.clear();

  QList<PersonProperty *> pproperties = adb->personProperties();
  Q_FOREACH (PersonProperty *property, pproperties) {

#if defined(EXAMPLE_DEBUG)
    qDebug() << property->property << ": " << property->lable;
#endif

  }
  qDeleteAll(pproperties);
  pproperties.clear();

  QList<GroupProperty *> gproperties = adb->groupProperties();
  Q_FOREACH (GroupProperty *property, gproperties) {

#if defined(EXAMPLE_DEBUG)
    qDebug() << property->property << ": " << property->lable;
#endif

  }
  qDeleteAll(gproperties);
  gproperties.clear();
}

ExampleWidget::~ExampleWidget() {

  if (adb) {
    delete adb;
  }

  delete ui;
}
