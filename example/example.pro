#/*
# * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * This file is part of QMacAddressBook.
# *
# * This library is free software: you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published
# * by the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this library.  If not, see <http://www.gnu.org/licenses/>.
# *
# */

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

INCLUDEPATH += $$PWD/../
DEPENDPATH += $$PWD/../

TEMPLATE = app
TARGET = Example
DESTDIR = ../build/bin
CONFIG += qt war_on thread silent

QMAKE_PRE_LINK = rm -rf $${DESTDIR}/$${TARGET}.app/Contents/Frameworks && mkdir -p $${DESTDIR}/$${TARGET}.app/Contents/Frameworks && cp -R $${DESTDIR}/QMacAddressBook.framework $${DESTDIR}/$${TARGET}.app/Contents/Frameworks/QMacAddressBook.framework
LIBS += -F$${DESTDIR}/$${TARGET}.app/Contents/Frameworks
LIBS += -framework QMacAddressBook

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += main.cpp
SOURCES += examplewidget.cpp
HEADERS += examplewidget.h
FORMS   += examplewidget.ui

CONFIG(debug, debug|release) {

  RCC_DIR = ../build/$${TARGET}/debug/rcc
  MOC_DIR += ../build/$${TARGET}/debug/moc
  OBJECTS_DIR += ../build/$${TARGET}/debug/obj
  UI_DIR += ../build/$${TARGET}/debug/ui

  DEFINES += EXAMPLE_DEBUG

} else {

  RCC_DIR = ../build/$${TARGET}/release/rcc
  MOC_DIR += ../build/$${TARGET}/release/moc
  OBJECTS_DIR += ../build/$${TARGET}/release/obj
  UI_DIR += ../build/$${TARGET}/release/ui

  DEFINES -= EXAMPLE_DEBUG
}

