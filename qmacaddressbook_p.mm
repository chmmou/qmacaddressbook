/*
 * Copyright © 2012 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of QMacAddressBook.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "qmacaddressbook_p.h"

namespace qmacaddressbook {

  QMacAddressBookPrivate::QMacAddressBookPrivate() {

    releasePool = [[NSAutoreleasePool alloc] init];
    addressBook = ABGetSharedAddressBook();
  }

  QMacAddressBookPrivate::~QMacAddressBookPrivate() {

    releaseCFObjects(addressBook);
    [releasePool release];
  }

  const CFArrayRef QMacAddressBookPrivate::persons() const {

    return ABCopyArrayOfAllPeople(addressBook);
  }

  const CFArrayRef QMacAddressBookPrivate::groups() const {

    return ABCopyArrayOfAllGroups(addressBook);
  }

  Person *QMacAddressBookPrivate::personForRecord(ABRecordRef personRecord) {

    Person *person = new Person;

    person->names = new PersonNames;
    person->information = new PersonInformation;

    person->personRecord = CFRetain(personRecord);;
    person->personRecordId = ABRecordCopyUniqueId(personRecord);
    person->personPropertyId = ABRecordCopyRecordType(personRecord);

    personNames(person, personRecord);
    personAddress(person, personRecord);
    personPhones(person, personRecord);
    personService(person, personRecord);
    personInformation(person, personRecord);
    personDates(person, personRecord);
    personUrls(person, personRecord);
    personGroup(person);

    releaseCFObjects(personRecord);

    return person;
  }

  PersonGroup *QMacAddressBookPrivate::groupForRecord(const ABGroupRef groupRecord) {

    PersonGroup *group = new PersonGroup;

    group->name = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(groupRecord, kABGroupNameProperty));
    group->groupRecord = CFRetain(groupRecord);
    group->groupRecordId = ABRecordCopyUniqueId(groupRecord);
    group->groupPropertyId = ABRecordCopyRecordType(groupRecord);

    releaseCFObjects(groupRecord);

    return group;
  }

  /** This is the static function used internally by Qt (from src/corelib/kernel/qcore_mac.cpp): */
  QString QMacAddressBookPrivate::qstringFromCFStringRef(CFStringRef str) {

    if (!str) {
      return QString();
    }

    CFIndex length = CFStringGetLength(str);
    if (length == 0) {
      return QString();
    }

    QString string(length, Qt::Uninitialized);
    CFStringGetCharacters(str, CFRangeMake(0, length), reinterpret_cast<UniChar *>(const_cast<QChar *>(string.unicode())));

    if (str) {
      CFRelease(str);
    }

    return string;
  }

  CFStringRef QMacAddressBookPrivate::qstringToCFStringRef(const QString &string) {

    return CFStringCreateWithCharacters(kCFAllocatorDefault,
                                        reinterpret_cast<const UniChar *>(string.unicode()),
                                        string.length());
  }

  void QMacAddressBookPrivate::releaseCFObjects(CFTypeRef object) {

    if (object) {
      CFRelease(object);
      object = 0x0;
    }
  }

  const QDate QMacAddressBookPrivate::qdateFromCFDateRef(CFDateRef date) const {

    if (!date) {
      return QDate();
    }

    QDateTime dt;
    dt.setTime_t((uint)kCFAbsoluteTimeIntervalSince1970);
    dt = dt.addSecs((int)CFDateGetAbsoluteTime(static_cast<CFDateRef>(date)));

    CFRelease(date);

    return dt.date();
  }

  const CFStringRef QMacAddressBookPrivate::searchComparisonProperty(const SearchComparisonProperty comparisonProperty) const {

    switch (comparisonProperty) {
      case FirstName:
        return kABFirstNameProperty;
      case LastName:
        return kABLastNameProperty;
      case FirstNamePhonetic:
        return kABFirstNamePhoneticProperty;
      case LastNamePhonetic:
        return kABLastNamePhoneticProperty;
      case Nickname:
        return kABNicknameProperty;
      case MaidenName:
        return kABMaidenNameProperty;
      case Organization:
        return kABOrganizationProperty;
      case JobTitle:
        return kABJobTitleProperty;
      case HomePage:
        return kABHomePageProperty;
      case Department:
        return kABDepartmentProperty;
      case MiddleName:
        return kABMiddleNameProperty;
      case MiddleNamePhonetic:
        return kABMiddleNamePhoneticProperty;
      case Title:
        return kABTitleProperty;
      case Suffix:
        return kABSuffixProperty;
      default:
        return kABFirstNameProperty;
    }
  }

#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_7
  void QMacAddressBookPrivate::personServiceInternal(Person *person, const ABRecordRef record, const CFStringRef property) {

    const ABMultiValueRef imServiceMultiValue = (ABMultiValueRef)ABRecordCopyValue(record, property);
    const CFIndex imServiceCount = ABMultiValueCount(imServiceMultiValue);
    if (imServiceMultiValue) {
      imServiceCount = ABMultiValueCount(imServiceMultiValue);
      for (CFIndex a = 0; a < imServiceCount; ++a) {

        PersonService *personService = new PersonService;
        if (property == kABAIMInstantProperty) {
          personService->aim = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(imServiceMultiValue, a));
        } else if (property == kABJabberInstantProperty) {
          personService->jabber = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(imServiceMultiValue, a));
        } else if (property == kABMSNInstantProperty) {
          personService->msn = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(imServiceMultiValue, a));
        } else if (property == kABYahooInstantProperty) {
          personService->yahoo = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(imServiceMultiValue, a));
        } else if (property == kABICQInstantProperty) {
          personService->icq = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(imServiceMultiValue, a));
        }
        personService->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(imServiceMultiValue, a));
        person->services.append(personService);
      }
    }
  }
#endif

  void QMacAddressBookPrivate::personNames(Person *person, const ABRecordRef personRecord) {

    person->names->firstName = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABFirstNameProperty));
    person->names->lastName = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABLastNameProperty));
    person->names->middleName = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABMiddleNameProperty));
    person->names->prefix = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABTitleProperty));
    person->names->suffix = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABSuffixProperty));
    person->names->nickName = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABNicknameProperty));
    person->names->firstNamePhonetic = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABFirstNamePhoneticProperty));
    person->names->lastNamePhonetic = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABLastNamePhoneticProperty));
    person->names->middleNamePhonetic = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABMiddleNamePhoneticProperty));
  }

  void QMacAddressBookPrivate::personAddress(Person *person, const ABRecordRef personRecord) {

    person->addresses = QList<PersonAddress *>();

    CFDictionaryRef address = 0x0;
    const ABMultiValueRef addresses = (ABMultiValueRef)ABRecordCopyValue(personRecord, kABAddressProperty);
    const CFIndex addressCount = ABMultiValueCount(addresses);
    for (CFIndex a = 0; a < addressCount; ++a) {

      PersonAddress *personAddress = new PersonAddress;
      address = (CFDictionaryRef)ABMultiValueCopyValueAtIndex(addresses, a);
      if (address) {

        personAddress->completeAddress = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressStreetKey));
        personAddress->street = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressStreetKey));
        personAddress->zipCode = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressZIPKey));
        personAddress->city = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressCityKey));
        personAddress->country = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressCountryKey));
        personAddress->countryCode = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressCountryCodeKey));
        personAddress->state = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(address, kABAddressStateKey));
        personAddress->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(addresses, a));

        person->addresses.append(personAddress);

        releaseCFObjects(address);
      }
    }
  }

  void QMacAddressBookPrivate::personPhones(Person *person, const ABRecordRef personRecord) {

    person->phones = QList<PersonPhone *>();

    const ABMultiValueRef phones = (ABMultiValueRef)ABRecordCopyValue(personRecord, kABPhoneProperty);
    if (phones) {

      const CFIndex phoneCount = ABMultiValueCount(phones);
      for (CFIndex a = 0; a < phoneCount; ++a) {

        PersonPhone *personPhone = new PersonPhone;
        personPhone->number = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(phones, a));
        personPhone->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(phones, a));

        person->phones.append(personPhone);
      }
    }
  }

  void QMacAddressBookPrivate::personUrls(Person *person, const ABRecordRef personRecord) {

    person->urls = QList<PersonUrl *>();

    ABMultiValueRef urls = (ABMultiValueRef)ABRecordCopyValue(personRecord, kABURLsProperty);
    if (urls) {
      const CFIndex urlCount = ABMultiValueCount(urls);
      for (CFIndex a = 0; a < urlCount; ++a) {

        PersonUrl *personUrl = new PersonUrl;
        personUrl->hompage = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(urls, a));
        personUrl->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(urls, a));

        person->urls.append(personUrl);
      }
      CFRelease(urls);
    }

    urls = (ABMultiValueRef)ABRecordCopyValue(personRecord, kABEmailProperty);
    if (urls) {
      const CFIndex urlCount = ABMultiValueCount(urls);
      for (CFIndex a = 0; a < urlCount; ++a) {

        PersonUrl *personUrl = new PersonUrl;
        personUrl->email = qstringFromCFStringRef((CFStringRef)ABMultiValueCopyValueAtIndex(urls, a));
        personUrl->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(urls, a));

        person->urls.append(personUrl);
      }

      CFRelease(urls);
    }
  }

  void QMacAddressBookPrivate::personService(Person *person, const ABRecordRef personRecord) {

    person->services = QList<PersonService *>();

#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_7
    /**
     * Thats support Mac OS X 10.6 and below
     */
    personServiceInternal(person, personRecord, kABAIMInstantProperty);
    personServiceInternal(person, personRecord, kABJabberInstantProperty);
    personServiceInternal(person, personRecord, kABMSNInstantProperty);
    personServiceInternal(person, personRecord, kABYahooInstantProperty);
    personServiceInternal(person, personRecord, kABICQInstantProperty);
#else
    const ABMultiValueRef imServiceMultiValue = (ABMultiValueRef)ABRecordCopyValue(personRecord, kABInstantMessageProperty);
    if (imServiceMultiValue) {

      const CFIndex imServiceCount = ABMultiValueCount(imServiceMultiValue);
      for (CFIndex a = 0; a < imServiceCount; ++a) {

        CFDictionaryRef imServices = (CFDictionaryRef)ABMultiValueCopyValueAtIndex(imServiceMultiValue, a);
        if (imServices) {

          PersonService *personService = new PersonService;

          personService->userName = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageUsernameKey));
          personService->service = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceKey));
          personService->aim = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceAIM));
          personService->facebook = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceFacebook));
          personService->gadugadu = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceGaduGadu));
          personService->googleTalk = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceGoogleTalk));
          personService->icq = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceICQ));
          personService->jabber = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceJabber));
          personService->msn = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceMSN));
          personService->qq = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceQQ));
          personService->skype = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceSkype));
          personService->yahoo = qstringFromCFStringRef((CFStringRef)CFDictionaryGetValue(imServices, kABInstantMessageServiceYahoo));
          personService->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(imServiceMultiValue, a));

          person->services.append(personService);

          CFRelease(imServices);
        }
      }
    }
#endif
  }

  void QMacAddressBookPrivate::personInformation(Person *person, const ABRecordRef personRecord) {

    person->information->organization = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABOrganizationProperty));
    person->information->department = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABDepartmentProperty));
    person->information->jobTitle = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABJobTitleProperty));
    person->information->note = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(personRecord, kABNoteProperty));
    person->information->birthday = qdateFromCFDateRef((CFDateRef)ABRecordCopyValue(personRecord, kABBirthdayProperty));
    person->information->creationDate = qdateFromCFDateRef((CFDateRef)ABRecordCopyValue(personRecord, kABCreationDateProperty));
    person->information->modificationDate = qdateFromCFDateRef((CFDateRef)ABRecordCopyValue(personRecord, kABModificationDateProperty));

    CFNumberRef num = (CFNumberRef)ABRecordCopyValue(personRecord, kABPersonFlags);
    int kCompany = kABShowAsPerson;
    if (CFNumberGetValue(num, kCFNumberIntType, &kCompany)) {
      if(kCompany == kABShowAsCompany) {
        person->information->isCompany = true;
      } else {
        person->information->isCompany = false;
      }
    } else {
      person->information->isCompany = false;
    }
    releaseCFObjects(num);
  }

  void QMacAddressBookPrivate::personDates(Person *person, const ABRecordRef personRecord) {

    person->dates = QList<PersonDate *>();

    const ABMultiValueRef dates = (ABMultiValueRef)ABRecordCopyValue(personRecord, kABOtherDatesProperty);
    if (dates) {
      const CFIndex dateCount = ABMultiValueCount(dates);
      for (CFIndex a = 0; a < dateCount; ++a) {

        PersonDate *personDate = new PersonDate;

        personDate->date = qdateFromCFDateRef((CFDateRef)ABMultiValueCopyValueAtIndex(dates, a));
        personDate->label = qstringFromCFStringRef(ABMultiValueCopyLabelAtIndex(dates, a));

        person->dates.append(personDate);
      }
    }
  }

  void QMacAddressBookPrivate::personGroup(Person *person) {

    if (person->groups.count() > 0) {
      qDeleteAll(person->groups);
      person->groups.clear();
    }

    person->groups = QList<PersonGroup *>();

    CFArrayRef groupRefs = ABPersonCopyParentGroups((ABPersonRef)person->personRecord);
    const CFIndex groupCount = CFArrayGetCount(groupRefs);
    for (CFIndex a = 0; a < groupCount; ++a) {

      ABGroupRef groupRecordRef = (ABGroupRef)CFArrayGetValueAtIndex(groupRefs, a);
      if (groupRecordRef) {

        PersonGroup *personGroup = new PersonGroup;
        personGroup->groupRecord = ABRecordCreateCopy(groupRecordRef);
        personGroup->groupRecordId = ABRecordCopyUniqueId(groupRecordRef);
        personGroup->groupPropertyId = ABRecordCopyRecordType(groupRecordRef);
        personGroup->name = qstringFromCFStringRef((CFStringRef)ABRecordCopyValue(groupRecordRef, kABGroupNameProperty));

        person->groups.append(personGroup);
      }
      releaseCFObjects(groupRecordRef);
    }
    releaseCFObjects(groupRefs);
  }

  void QMacAddressBookPrivate::setPersonNames(Person *person, const ABRecordRef record)
  {
  }

  void QMacAddressBookPrivate::setPersonAddress(Person *person, const ABRecordRef record)
  {
  }

  void QMacAddressBookPrivate::setPersonPhones(Person *person, const ABRecordRef record)
  {
  }

  void QMacAddressBookPrivate::setPersonService(Person *person, const ABRecordRef record)
  {
  }

  void QMacAddressBookPrivate::setPersonDates(Person *person, const ABRecordRef record)
  {
  }

  void QMacAddressBookPrivate::setPersonInformation(Person *person, const ABRecordRef record)
  {
  }

  void QMacAddressBookPrivate::setPersonGroup(Person *person, const ABRecordRef record)
  {
  }

  const CFArrayRef QMacAddressBookPrivate::personsForName(const CFStringRef value, const SearchComparisonProperty comparisonProperty, const SearchComparison comparison) const {

    ABSearchComparison searchComparison = kABContainsSubStringCaseInsensitive;
    switch (comparison) {
      case Equal:
        searchComparison = kABEqual;
        break;
      case EqualCaseInsensitive:
        searchComparison = kABEqualCaseInsensitive;
        break;
      case Contains:
        searchComparison = kABContainsSubString;
        break;
      case ContainsCaseInsensitive:
        searchComparison = kABContainsSubStringCaseInsensitive;
        break;
    }

    return ABCopyArrayOfMatchingRecords(addressBook,
                                        ABPersonCreateSearchElement(
                                          searchComparisonProperty(comparisonProperty),
                                          NULL,
                                          NULL,
                                          value,
                                          searchComparison
                                        )
                                       );
  }

  const CFArrayRef QMacAddressBookPrivate::personsForGroup(const CFStringRef property, const CFStringRef group, const SearchComparison comparison) const {
  }

  const CFArrayRef QMacAddressBookPrivate::properties(const CFStringRef recordType) const {

    return ABCopyArrayOfPropertiesForRecordType(addressBook, recordType);
  }

  bool QMacAddressBookPrivate::addPersonRecord(const ABRecordRef personRecord) const {

    bool added = false;

    if (personRecord) {
      added = ABAddRecord(addressBook, personRecord);
    }

    return added;
  }

  bool QMacAddressBookPrivate::addGroupRecord(const ABGroupRef groupRecord) const {

    bool added = false;

    if (groupRecord) {
      added = ABAddRecord(addressBook, groupRecord);
    }

    return added;
  }

  Person *QMacAddressBookPrivate::preparePersonRecord(const Person *person) {

    if (!person) {
      return 0;
    }

    Person *_person = const_cast<Person *>(person);
    if (!_person->personRecord) {
      _person->personRecord = (ABRecordRef)ABPersonCreate();
    }

    _person->personRecordId = ABRecordCopyUniqueId((ABRecordRef)_person->personRecord);
    _person->personPropertyId = ABRecordCopyRecordType((ABRecordRef)_person->personRecord);

    return _person;
  }

  PersonGroup *QMacAddressBookPrivate::prepareGroupRecord(const PersonGroup *group) {

    if (!group) {
      return 0;
    }

    PersonGroup *_group = const_cast<PersonGroup *>(group);
    if (!_group->groupRecord) {
      _group->groupRecord = (ABGroupRef)ABGroupCreate();
    }

    _group->groupRecordId = ABRecordCopyUniqueId((ABGroupRef)_group->groupRecord);
    _group->groupPropertyId = ABRecordCopyRecordType((ABGroupRef)_group->groupRecord);

    return _group;
  }

  void QMacAddressBookPrivate::setPersonToRecord(const Person *person) {
  }

  void QMacAddressBookPrivate::setGroupToRecord(const PersonGroup *group) {
  }

  bool QMacAddressBookPrivate::hasUnsavedChanges() {

    return ABHasUnsavedChanges(addressBook);
  }

  bool QMacAddressBookPrivate::save() {

    return ABSave(addressBook);
  }

}
